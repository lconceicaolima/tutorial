#include "fatorial.h"

int fatorial(int x){

	/* Escreva seu código aqui */

	if (x<=0) { //retorna -1 para valores inválidos
		return -1;
	}

	if (x==1) { //calcula o fatorial
		return 1;
	} else {
		return (x * fatorial(x-1));
	}
}


